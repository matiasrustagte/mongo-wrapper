const mongodb = require('mongodb');
const ObjectId = mongodb.ObjectId;
const EventEmitter = require('events');

class MongoModel {
  /**
   * Database collection name.
   * @returns {string}
   */
  static get collectionName () {
    return this.name.toLowerCase();
  }

  /**
   * Database collection schema.
   * @link https://docs.mongodb.com/manual/reference/operator/query/jsonSchema/
   * @returns {Object}
   */
  static get jsonSchema () {
    return {};
  }

  /**
   * @return {Object}
   */
  static get defaults () {
    return {};
  }

  /**
   * Database collection indexes.
   * Example: [{ keys: { email: 1 }, options: { unique: true } }, ...]
   * @returns {Array.<Object>}
   */
  static get indexes () {
    return [];
  }

  /**
   * @returns {EventEmitter}
   */
  static get events () {
    if (!this._events) {
      this._events = new EventEmitter();
      Object.defineProperty(this, '_events', { writable: false });
    }
    return this._events;
  }

  /**
   * @param {Object} options
   * @param {mongodb.Db} db Native mongodb driver's database connection.
   * @returns {Promise}
   */
  static async initialize ({ db }) {
    this.collection = db.collection(this.collectionName);
    Object.defineProperty(this, 'collection', { writable: false });

    this._objectIdFields = [];
    this._dateFields = [];
    const properties = this.jsonSchema.properties;
    if (properties) {
      for (let key in properties) {
        if (properties[key].bsonType === 'objectId')
          this._objectIdFields.push(key);
        else if (properties[key].bsonType === 'date')
          this._dateFields.push(key);
      }
    }
    this._fields = properties ? Object.keys(properties) : [];
  }

  /**
   * @param {Object} options
   * @param {mongodb.Db} db Native mongodb driver's database connection.
   * @returns {Promise}
   */
  static async updateDB ({ db }) {
    if (!this.collection)
      this.collection = db.collection(this.collectionName);

    // @todo use db.getCollectionInfos and db.collection.getIndexes before
    await db.createCollection(this.collectionName);
    await db.command({
      collMod: this.collectionName,
      validator: { $jsonSchema: this.jsonSchema }
    });
    for (let index of this.indexes)
      await this.collection.createIndex(index.keys, index.options);
  }

  /**
   * @param {string} event
   * @param {Function} cb
   */
  static on () {
    this.events.on(...arguments);
  }

  /**
   * @typedef {ObjectId|string} MongoModel.ObjectIdLike
   */

  /**
   * @param {MongoModel.ObjectIdLike} id
   * @returns {Promise.<?MongoModel>}
   */
  static async get (id) {
    const doc = await this.collection.findOne({ _id: ObjectId(id) });
    return doc ? new this(doc) : null;
  }

  /**
   * @param {MongoModel.ObjectIdLike} id
   * @returns {Promise.<?MongoModel>}
   * @throws {Error}
   */
  static async getStrict (id) {
    const doc = await this.collection.findOne({ _id: ObjectId(id) });
    if (!doc) throw Error(this.name.toUpperCase() + '_NOT_FOUND');
    return new this(doc);
  }

  /**
   * @typedef {Object} MongoModel.ListOptions
   * @property {Object.<string, (1|-1)>=} sort
   * @property {number=} limit
   */

  /**
   * @param {Object=} where
   * @param {MongoModel.ListOptions=} options
   * @return {Promise.<Array.<MongoModel>>}
   */
  static async list (where = {}, { sort, limit } = {}) {
    for (let key of this._objectIdFields) {
      if (key in where) {
        if (Array.isArray(where[key]))
          where[key] = where[key].map(id => ObjectId(id));
        else if (typeof where[key] === 'string')
          where[key] = ObjectId(where[key]);
      }
    }
    for (let key of this._dateFields) {
      if (key in where) {
        if (Array.isArray(where[key]))
          where[key] = where[key].map(date => new Date(date));
        else if (typeof where[key] === 'string')
          where[key] = new Date(where[key]);
      }
    }
    for (let key in where) {
      if (Array.isArray(where[key]))
        where[key] = { $in: where[key] };
    }
    let query = this.collection.find(where);
    if (sort) query = query.sort(sort);
    if (limit) query = query.limit(limit);
    const docs = await query.toArray();
    return docs.map(doc => new this(doc));
  }

  static async isEmpty (where = {}) {
    const [doc] = await this.list(where, { limit: 1 });
    return !doc;
  }

  /**
   * @param {MongoDocument} doc
   * @param {ObjectId=} doc._id
   * @constructor
   */
  constructor (doc = {}) {
    const originalDoc = doc;
    doc = {};
    for (let key of this.constructor._fields) {
      if (key in originalDoc && originalDoc[key] !== undefined)
        doc[key] = originalDoc[key];
    }

    if (!doc._id) {
      for (let key of this.constructor._objectIdFields) {
        if (key in doc)
          doc[key] = ObjectId(doc[key]);
      }
      for (let key of this.constructor._dateFields) {
        if (key in doc)
          doc[key] = new Date(doc[key]);
      }
    }

    Object.assign(this, this.constructor.defaults, doc);
  }

  /**
   * @returns {string|undefined}
   */
  get id () {
    return this._id ? this._id.toString() : undefined;
  }

  toJSON () {
    const { _id, ...obj } = this;
    obj.id = this.id;
    return obj;
  }

  async save () {
    if (this._id)
      throw Error('CANNOT_CREATE_DUPLICATE');
    let data = {};
    try {
      for (let key in this) {
        if (this[key] !== undefined)
          data[key] = this[key];
      }
      const result = await this.constructor.collection.insertOne(data);
      this._id = result.insertedId;
    } catch (e) {
      throw {
        message: 'VALIDATION_FAILED',
        model: this.constructor.name,
        operation: 'insertOne',
        data: data,
      };
    }
    this.emit('created');
  }

  async remove () {
    await this.constructor.collection.remove({ _id: this._id });
    this.emit('deleted');
  }

  /**
   * @param {Object} changes
   */
  async update (changes) {
    if (!this._id)
      throw Error('YOU_MUST_SAVE_BEFORE_UPDATE');

    const _changes = {};
    for (let key in changes) {
      if (changes[key] !== undefined)
        _changes[key] = changes[key];
    }
    if ('_id' in _changes)
      delete _changes._id;

    await this.applyChanges(_changes);
  }

  /**
   * @param {Object} changes
   * @protected
   */
  async applyChanges (changes) {
    try {
      if (!Object.keys(changes).length)
        return;
      for (let key of this.constructor._objectIdFields) {
        if (key in changes && changes[key])
          changes[key] = ObjectId(changes[key]);
      }
      for (let key of this.constructor._dateFields) {
        if (key in changes && changes[key])
          changes[key] = new Date(changes[key]);
      }
      await this.constructor.collection.updateOne({ _id: this._id }, { $set: changes });
      Object.assign(this, changes);
    } catch (e) {
      throw {
        message: 'VALIDATION_FAILED',
        model: this.constructor.name,
        operation: 'updateOne',
        _id: this._id,
        data: changes,
      };
    }
    this.emit('updated', changes);
  }

  /**
   * Updates object from db in-place.
   */
  async reload () {
    const doc = await this.constructor.collection.findOne({ _id: this._id });
    for (let key in this) {
      if (!(key in doc))
        delete this[key];
    }
    Object.assign(this, doc);
  }

  /**
   * @param {string} event
   * @param {...*} args
   */
  emit (event, ...args) {
    this.constructor.events.emit(event, this, ...args);
  }
}

module.exports = MongoModel;