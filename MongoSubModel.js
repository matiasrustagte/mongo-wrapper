class MongoSubModel {
  static get jsonSchema () {
    return {};
  }

  static get defaults () {
    return {};
  }

  static async initialize ({ db }) {
    this._objectIdFields = [];
    this._dateFields = [];
    const properties = this.jsonSchema.properties;
    if (properties) {
      for (let key in properties) {
        if (properties[key].bsonType === 'objectId')
          this._objectIdFields.push(key);
        else if (properties[key].bsonType === 'date')
          this._dateFields.push(key);
      }
    }
    this._fields = properties ? Object.keys(properties) : [];
  }

  /**
   * @param {MongoDocument} doc
   * @param {ObjectId=} doc._id
   * @constructor
   */
  constructor (doc = {}) {
    const originalDoc = doc;
    doc = {};
    for (let key of this.constructor._fields) {
      if (key in originalDoc && originalDoc[key] !== undefined)
        doc[key] = originalDoc[key];
    }

    if (!doc._id) {
      for (let key of this.constructor._objectIdFields) {
        if (key in doc)
          doc[key] = ObjectId(doc[key]);
      }
      for (let key of this.constructor._dateFields) {
        if (key in doc)
          doc[key] = new Date(doc[key]);
      }
    }

    Object.assign(this, this.constructor.defaults, doc);
  }

  /**
   * @returns {string|undefined}
   */
  get id () {
    return this._id ? this._id.toString() : undefined;
  }

  toJSON () {
    const { _id, ...obj } = this;
    obj.id = this.id;
    return obj;
  }
}

module.exports = MongoSubModel;